using System.Runtime.Serialization;

namespace Contracts
{
    [DataContract(Namespace = "Service", Name = "Person")]
    public class Person
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
