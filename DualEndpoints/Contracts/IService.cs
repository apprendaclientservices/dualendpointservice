using System.ServiceModel;
using System.ServiceModel.Web;

namespace Contracts
{
    [ServiceContract(Namespace = "Service", Name = "Service")]
    public interface IService
    {
        [OperationContract]
        DataTransferObject SomeMethod(DataTransferObject data);

        [OperationContract]
        Person GetData(string id);

    }

    [ServiceContract(Namespace = "Service", Name = "Service1")]
    public interface IService1
    {
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "data/{id}")]
        Person GetData(string id);
    }
}
