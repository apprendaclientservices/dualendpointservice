﻿using System;
using Apprenda.SaaSGrid;
using root.Proxies;

namespace root
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }
    

        protected void OnServiceCallButtonClick(object sender, EventArgs args)
        {
            var p = new Proxy();
            statusMessage.Text = "Call completed. Result was " + p.GetData(ID_TextBox.Text).Name;
            p.Close();
        }
    }
}
