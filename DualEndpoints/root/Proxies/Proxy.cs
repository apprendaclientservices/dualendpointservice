﻿using System.ServiceModel;
using Contracts;

namespace root.Proxies
{
    internal class Proxy : ClientBase<IService>, IService
    {
        public DataTransferObject SomeMethod(DataTransferObject data)
        {
            return base.Channel.SomeMethod(data);
        }

        public Person GetData(string id)
        {
            return base.Channel.GetData(id);
        }
    }
}
